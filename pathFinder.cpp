#include <iostream>
#include <iomanip>
#include <queue>
#include <list>
#include <string>
#include <limits>

#define MAP_ROWS 10
#define MAP_COLS 10
#define MAX_VAL numeric_limits<int>::max()

using namespace std;

int workspace[MAP_ROWS][MAP_COLS];
int visitedNodes[MAP_ROWS][MAP_COLS];
int result[MAP_ROWS][MAP_COLS];

struct node {
    int row;
    int col;
};

queue <node> q;
list <node> shorthestPath;
struct node goalPoint;
struct node startPoint;

void initWorkspace() {

// init goal and start points
	goalPoint.row = -1;
	goalPoint.col = -1;

	startPoint.row = -1;
	startPoint.col = -1;


// init obstacles
// second row 3 cells, horizontal line
	workspace[1][0] = -1;
	workspace[1][1] = -1;
	workspace[1][2] = -1;
	visitedNodes[1][0] = -1;
	visitedNodes[1][1] = -1;
	visitedNodes[1][2] = -1;

// forth row 3 cells, horizontal line
	workspace[3][0] = -1;
	workspace[3][1] = -1;
	workspace[3][2] = -1;
	workspace[2][2] = -1;
	visitedNodes[3][0] = -1;
	visitedNodes[3][1] = -1;
	visitedNodes[3][2] = -1;
	visitedNodes[2][2] = -1;
	
// 5th col 3 cells, vertical line
	workspace[2][5] = -1;
	workspace[3][5] = -1;
	workspace[4][5] = -1;
	visitedNodes[2][5] = -1;
	visitedNodes[3][5] = -1;
	visitedNodes[4][5] = -1;

// 8th row 3 cells, horizontal line
	workspace[8][6] = -1;
	workspace[8][7] = -1;
	workspace[8][8] = -1;
	visitedNodes[8][6] = -1;
	visitedNodes[8][7] = -1;
	visitedNodes[8][8] = -1;

// diagonal line from bottom-left to top-right
	workspace[8][0] = -1;
	workspace[7][1] = -1;
	workspace[6][2] = -1;
	visitedNodes[8][0] = -1;
	visitedNodes[7][1] = -1;
	visitedNodes[6][2] = -1;	
	
}

// function responsible for creating navigation values across workspace
void visitNextNodes(int row, int col) {

	int val = workspace[row][col];

	if ( workspace[row][col] != -1 && visitedNodes[row][col] != 1 && row >= 0 && row < MAP_ROWS && col >= 0 && col < MAP_COLS ) {
	
		val++;
		visitedNodes[row][col] = 1;

		// up
		if( row - 1 >= 0 && workspace[row - 1][col] != -1 && workspace[row - 1][col] == 0 && visitedNodes[row - 1][col] != 1 ) {
			workspace[row - 1][col] = val;
			q.push( { row-1, col } );
		}

		// down
		if( row + 1 < MAP_ROWS && workspace[row + 1][col] != -1 && workspace[row + 1][col] == 0 && visitedNodes[row + 1][col] != 1 ) {
			workspace[row + 1][col] = val;
			q.push( { row+1, col } );
		}

		// left
		if( col - 1 >= 0 && workspace[row][col - 1] != -1 && workspace[row][col - 1] == 0 && visitedNodes[row][col - 1] != 1 ) {
			workspace[row][col - 1] = val;
			q.push( { row, col-1 } );
		}
		
		// right
		if( col + 1 < MAP_COLS && workspace[row][col + 1] != -1 && workspace[row][col + 1] == 0 && visitedNodes[row][col + 1] != 1 ) {
			workspace[row][col + 1] = val;
			q.push( { row, col+1 } );
		}
	}
}

void navFoo(int goalRow, int goalCol) {

	goalPoint.row = goalRow;
	goalPoint.col = goalCol;

	q.push( {goalRow, goalCol} );
	struct node nodeToExplore;

	while ( !q.empty() ) {

		nodeToExplore = q.front();
		q.pop();
		visitNextNodes(nodeToExplore.row, nodeToExplore.col);
	}
}

void findShortestPath(int startRow, int startCol) {

	startPoint.row = startRow;
	startPoint.col = startCol;
	shorthestPath.push_back( {startRow, startCol} );

	int min;
	struct node nextNode;
	int row = startRow;
	int col = startCol;

	
	while ( workspace[row][col] != 0) {

		min = MAX_VAL;
		nextNode.row = -1;
		nextNode.col = -1;

		// up
		if( row - 1 >= 0 && workspace[row - 1][col] != -1 ) {
			if ( workspace[row - 1][col] < min ) {
				min = workspace[row - 1][col];
				nextNode.row = row - 1;
				nextNode.col = col;		
			}	
		}

		// down
		if( row + 1 < MAP_ROWS && workspace[row + 1][col] != -1 ) {
			if ( workspace[row + 1][col] < min ) {
				min = workspace[row + 1][col];
				nextNode.row = row + 1;
				nextNode.col = col;		
			}
		}

		// left
		if( col - 1 >= 0 && workspace[row][col - 1] != -1 ) {
			if ( workspace[row][col - 1] < min ) {
				min = workspace[row][col - 1];
				nextNode.row = row;
				nextNode.col = col - 1;		
			}
		}
		
		// right
		if( col + 1 < MAP_COLS && workspace[row][col + 1] != -1 ) {
			if ( workspace[row][col + 1] < min ) {
				min = workspace[row][col + 1];
				nextNode.row = row;
				nextNode.col = col + 1;		
			}
		}	

		// not possible to move to next node
		if ( nextNode.row == -1 ) {
			break;
		} else {
			row = nextNode.row;
			col = nextNode.col;

			shorthestPath.push_back( nextNode );
		}
	}
}

void showWorkspace() {

	cout << endl;
	// show cols description
	for (int k = 0; k <= MAP_COLS; ++k) {
			if ( k == 0 )
				cout << setw(3) << "";
			else
				cout << setw(3) << k-1 << '.';
	}
	cout << endl;

	for (int i = 0; i < MAP_ROWS; ++i) {

		// write grid lines
		for (int k = 0; k <= MAP_COLS; ++k) {
			
			// first row just horizontal lines
			if ( i == 0 && k != 0 ) {
				cout << setw(4) << "___";
			} else if ( k != 0 ) {
				cout << setw(4) << "|___";
				// finish row with just vertical line
				if ( k == MAP_COLS )
					cout << "|";	
				// make room for row number description
			} else 
				cout << setw(4) << "";	
		}
		cout << endl;
		cout << setw(3) << i << ".";	// write row number
		
		// write workspace values
		for (int j = 0; j < MAP_COLS; ++j) {
			
			// obstacle - red
			if ( workspace[i][j] == -1 )
				cout << "|" << "\033" << "[31m" << setw(3) << "###" << "\033" << "[0m";
			// start/goal point
			else if ( (i == goalPoint.row && j == goalPoint.col) || (i == startPoint.row && j == startPoint.col) )	
				cout << "|" << "\033" << "[32m" << setw(3) << workspace[i][j] << "\033" << "[0m";
			// solution path - cyan
			else if ( result[i][j] == 1 )	
				cout << "|" << "\033" << "[36m" << setw(3) << workspace[i][j] << "\033" << "[0m";
			// nor obstacle nor solution path - white
			else
				cout << "|" << setw(3) << workspace[i][j];
			// finish row with vertical line
			if ( j == MAP_COLS - 1)
				cout << "|";
		}
		cout << endl;

		// add last row with lines to close grid 
		if ( i == MAP_ROWS - 1) {
			for (int l = 0; l <= MAP_COLS; ++l) {
				
				if ( l == 0 )
					cout << setw(4) << "";
				else
					cout << setw(4) << "|___";

				if ( l == MAP_COLS )
					cout << "|";
			}
		}
	}
	cout << endl;
}

void showVisitedNodes() {

	cout << endl;
	for (int i = 0; i < MAP_ROWS; ++i) {
		for (int j = 0; j < MAP_COLS; ++j) {
			
			cout << visitedNodes[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

void showShorthestPath() {

	// cout << endl;
	// for (int i = 0; i < MAP_ROWS; ++i) {
	// 	for (int j = 0; j < MAP_COLS; ++j) {
	// 		cout << result[i][j] << " ";
	// 	}
	// 	cout << endl;
	// }
	// cout << endl;

	struct node lastNode = shorthestPath.back();

	if ( lastNode.row == goalPoint.row && lastNode.col == goalPoint.col ) {
		for (list<node>::iterator it = shorthestPath.begin(); it != shorthestPath.end(); it++) {
			result[it->row][it->col] = 1;
		}
		showWorkspace();
	} else {
		cout << endl << "  Not possible to find solution path to point (" << goalPoint.col << ", " << goalPoint.row << ") "
			 << "from point (" << startPoint.col << ", " << startPoint.row << ")" << endl << endl;
	}	
}

void description() {

	cout << endl << "  Color description:" << endl << endl;
	cout << "\033" << "[31m" << "  # (red)" << "\033" << "[0m" << "       - obstacle" << endl;
	cout << "\033" << "[36m" << "  value (blue)" << "\033" << "[0m" << "  - solution path" << endl;
	cout << "\033" << "[32m" << "  value (green)" << "\033" << "[0m" << " - goal/start point" << endl;
	cout << "  value (white) - navigation function" << endl;

}

int getCoordinates(string cmd, int &x, int &y) {

	cout << endl << "  " << cmd << endl;
	cout << "  x: ";
	cin >> x;

	// validate input is and integer
    if ( cin.fail() ) {
    
    	cin.clear();
    	cin.ignore(numeric_limits<streamsize>::max(),'\n');
	    return -1;

    } else {
    	cout << "  y: ";
    	cin >> y;
    	// validate input is and integer
    	if ( cin.fail() ) {
	    
	    	cin.clear();
	    	cin.ignore(numeric_limits<streamsize>::max(),'\n');
		    return -1;
		}	
    }
    
    // validate input is within workspace range
    if ( workspace[y][x] == -1 || x < 0 || x >= MAP_COLS || y < 0 || y >= MAP_ROWS )
    	return -1;
    
    return 0;
}

int main() {

	int x,y;

	// clear terminal
	// cout << "\033" << "[2J";
	
	description();
	initWorkspace();

	cout << endl << "		## Workspace ##" << endl;
    showWorkspace();

    // get start point coordinates
	while ( getCoordinates("Provide goal point coordinates", x, y) == -1 ) {
		cout << "  Wrong input. Point either is an obstacle or outside the workspace." << endl;
		// return 0;
	}

    navFoo(y, x);
    
    cout << endl << "	   ## Navigation finction ##";
    cout << endl << "	         ## (" << goalPoint.col << ", " << goalPoint.row << ") ##" << endl;
    
    showWorkspace();
    // get end point coordinates
    while ( getCoordinates("Provide start point coordinates", x, y) == -1 ) {
		cout << "  Wrong input. Point either is an obstacle or outside the workspace." << endl;
		// return 0;
	}
    
    findShortestPath(y, x);

    cout << endl << "	    ## Shorthest path ##";
    cout << endl << "	   ## (" << startPoint.col << ", " << startPoint.row << ") -> (" << goalPoint.col << ", " << goalPoint.row << ") ##" << endl;

    showShorthestPath();
}

