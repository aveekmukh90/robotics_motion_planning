
# Variables pour la compilation des fichiers
CC        =  g++
CFLAGS    =  -c -Wall -std=c++11

all: pathFinder

pathFinder: pathFinder.o
	$(CC)  pathFinder.o -o pathFinder

pathFinder.o: pathFinder.cpp
	$(CC) $(CFLAGS) pathFinder.cpp

clean:
	/bin/rm -f *.o
	/bin/rm -f pathFinder

run: all
	./pathFinder
